package net.agilelab.stg.apps.onlineshoppingapi.item;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(MockitoJUnitRunner.class)
public class ItemControllerTest {

    MockMvc mockController;
    @InjectMocks
    ItemController itemController;
    @Mock
    ItemRepo mockItemRepo;
    @Captor
    ArgumentCaptor<Item> itemArgCaptor;

    String json;
    String updateJson;

    @Before
    public void setUp() throws JsonProcessingException {
        mockController = standaloneSetup(itemController).build();

        Item item = new Item(
                "001",
                "title1",
                "description1",
                100
        );

        ObjectMapper mapper = new ObjectMapper();
        json
    }
}
