package net.agilelab.stg.apps.onlineshoppingapi.item;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/items")
@CrossOrigin
public class ItemController {

    private ItemRepo itemRepo;

    public ItemController(ItemRepo itemRepo) {
        this.itemRepo = itemRepo;
    }

    @GetMapping
    public List<Item> getAll() {
        return itemRepo.getAll();
    }
}
